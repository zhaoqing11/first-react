// 1.导入createContext()
// 2.使用<Context.Provider>包裹顶层元素
// 3.底层组件使用useContext(createContext返回的对象)
import { createContext } from 'react'

const Context = createContext()

export default Context
