import '../app.css'

const style = { color: 'green', fontSize: '12px'}

const active = true

function App() {
  return (
    <div className="App">

     <div className="App">
       <span style={style}>测试样式</span>
       <span className={ active ? 'active' : '' }>测试样式</span>
     </div>

    </div>
  );
}

export default App;
