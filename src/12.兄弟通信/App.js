import React from 'react'

/**
 * 兄弟通信
 * 1.通过子传父的方式定义一个函数，将要传递的参数作为函数的实参进行传递
 * 2.然后通过父传子的方式将函数中返回的参数赋值给对应变量传递给A组件
 */
function SonA({ msgA }) {
  return <div>child a,{msgA}</div>
}

function SonB({ getBMsg }) {
  return (
    <div>
      son b,
      <button onClick={() => getBMsg('hello')}>click</button>
    </div>
  )
}

class App extends React.Component {
  state = {
    msgA: 'init...'
  }
  getBMsg = val => {
    console.log(val)
    this.setState({
      msgA: val
    })
  }
  render() {
    return (
      <div>
        this is parent
        <SonA msgA={this.state.msgA} />
        <SonB getBMsg={this.getBMsg} />
      </div>
    )
  }
}

export default App
