import React, { useRef, useEffect } from 'react'

// 组件实例 类组件
// dom对象 标签
class TestC extends React.Component {
  state = {
    num: 20
  }
  getName = () => {
    return 'tom'
  }
  render() {
    return <div>hello</div>
  }
}

function App() {
  const testRef = useRef(null)
  const h1Ref = useRef(null)
  const btnRef = useRef(null)
  // useEffect回调 是在dom渲染之后执行
  useEffect(() => {
    console.log(testRef.current)
    console.log(h1Ref.current)
    console.log(btnRef.current)
  }, [])
  return (
    <div>
      <TestC ref={testRef} />
      <h1 ref={h1Ref}>this is child</h1>
      <button ref={btnRef}>click</button>
    </div>
  )
}

export default App
