// react 框架核心包
// react-dom 做渲染相关的包

import React from 'react'
import ReactDOM from 'react-dom/client'

// 应用的全局样式文件
import './index.css'

// 引入根组件
import App from './App'
// Context如需传递数据 只需在整个应用初始化时传递一次即可 然后就可在当前文件里做数据提供
// 如果Context需要传递数据且将来还需要对数据做修改 底层组件也跟着一起变 建议写道app.js
import Context from './context/context.js'

// 渲染根组件app 到一个id为root的节点上
const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
  // 去掉严格模式节点： <React.StrictMode>
  // useEffect的执行机制

  // <React.StrictMode>
  <Context.Provider value={10}>
    <App />
  </Context.Provider>
  // </React.StrictMode>
)
