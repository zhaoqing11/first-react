// 1.导入useNavigate
import { useNavigate } from 'react-router-dom'

function Login() {
  // 2.执行useNavigate得到一个跳转函数
  const navigate = useNavigate()
  // 跳转
  function goAbout() {
    // navigate('/about?id=100', { replace: true })
    navigate('/about/100')
  }
  return (
    <div>
      login
      <br />
      <button onClick={goAbout}>跳转</button>
    </div>
  )
}

export default Login
