import { useSearchParams, useParams } from 'react-router-dom'

function About() {
  // 方法一：
  // params是一个对象 对象里有一个get方法
  // 用来获取对应的参数 把参数的名称作为get方法的实参传过来
  // const [params] = useSearchParams()
  // const id = params.get('id')
  // const name = params.get('name')

  // 方法二：
  const params = useParams()
  console.log(params)

  return (
    <div>
      about
      <br />
      {/* id->{id},name->{name} */}
      id->{params.id}
    </div>
  )
}

export default About
