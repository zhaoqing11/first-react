import './app.css'
import React from 'react'

// 组件状态的定义与使用
class ComponentStatus extends React.Component {
  // 1.定义组件状态
  state = {
    // 在这可以定义各种属性，全都是当前组件状态
    name: '哈哈',
    count: 0,
    list: [1, 2, 3],
    person: {
      name: 'tom',
      age: 18
    }
  }
  // 事件回调函数
  changeName = () => {
    // 修改state中的name (注意*: 不可以直接做赋值修改 必须通过一个方法 setState)
    this.setState({
      name: '嘿嘿'
    })

    // 错误写法，不可直接赋值
    // this.state.count = 1
    // this.state.count++
    // ++this.state.count
    // this.state.list.push('add')
    // this.state.person.name = 'rose'

    // 正确写法
    // this.setState({
    //   count: this.state.count + 1,
    //   list: [...this.state.list, 'add'],
    //   person: {
    //     ...this.state.person,
    //     name: 'rose'
    //   }
    // })

    // setTimeout(() => {
    //   console.log('count->', this.state.count)
    //   console.log('list->', this.state.list)
    //   console.log('person->', this.state.person)
    // }, 2000)

    // 删除 filter
    this.setState({
      list: this.state.list.filter(item => item !== 2)
    })
  }
  render() {
    // 2.使用状态
    return (
      <>
        <div>{this.state.name}</div>
        <button onClick={this.changeName}>修改name值</button>
        <ul>
          {this.state.list.map((item, index) => (
            <li key={index}>{item}</li>
          ))}
        </ul>
        <div>count: {this.state.count}</div>
        <div>person: {this.state.person.name}</div>
      </>
    )
  }
}

/**
 * 总结
 * 1.编写组件其实就是编写原生js类或者函数
 * 2.定义状态必须通过state 实例属性的方法 提供一个对象 名称是固定的就叫做state
 * 3.修改state中的任何属性 都不可以通过直接赋值 必须走setState方法 这个方法来自于继承得到
 * 4.这里的this关键词，很容易出现指向错误的问题，上面的写法是最推荐和规范的，不会出现指向问题
 * (React状态: 不要直接修改状态的值，而是基于当前状态创建新的状态值)
 */

// 组件-this指向问题
class Test extends React.Component {
  // 使用bind强行修正this指向
  //（相当于在类组件初始化时，就可以把回调函数的this指向当前组件实例对象）
  constructor() {
    super()
    this.handle = this.handle.bind(this)
  }
  handle() {
    console.log(this)
  }
  // handle = () => {
  //   console.log(this)
  // }
  render() {
    return (
      <div>
        <button onClick={this.handle}>打印this</button>
        {/* 如果不用constructor做修正 
            可以直接在事件绑定位置通过箭头函数写法 
            直接沿用父函数中的this指向 */}
        <button onClick={() => this.handle()}>打印this2</button>
      </div>
    )
  }
}

function App() {
  return (
    <div className="App">
      {/* 渲染函数组件 */}
      <ComponentStatus />
      <Test />
    </div>
  )
}

export default App
