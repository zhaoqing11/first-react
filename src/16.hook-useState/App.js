// useState 快速使用
// 1.导入useState函数 react
// 2.执行这个函数并且传入初始值 必须在函数组件中
// 3.[数据，修改数据的方法]
// 4.使用数据 修改数据

// 状态的读取和修改
// const [count, setCount] = useState(0)
// 1.useState传过来的参数 作为count的初始值
// 2.[count, setCount] 解构赋值(名字可自定义保持语义化，参数顺序不可换) useState返回值是一个数组
// 3.setCount函数（基于原值计算得到新值） 用来修改count 不能直接修改原值还是生成一个新值替换原值
// 4.count和setCount是绑定在一起的 setcount只能用来修改对应的count值

// hook-useState 组件更新过程
// 当调用setCount的时候 更新过程

// 首次渲染
// 首次渲染的时候 组件内部的代码会被执行一次
// 其中useState也会跟着执行 重点注意 初始值只在首次渲染时生效

// 更新渲染 setCount都会更新
// 1.app组件会再次渲染 这个函数会再次执行
// 2.useState再次执行 得到的新的count值不是0而是修改之后的1 模板会用新值渲染

// 使用规则：只能在函数组件中使用 且组件最外层使用useState 不能出现在内部函数组件或if/else/for中使用
import { useState } from 'react'
import { useWindowScroll } from '../hook/useWinScroll'

// hook-useState 函数作为参数
function Counter(props) {
  const [count, setCount] = useState(() => {
    return props.count
  })
  return <button onClick={() => setCount(count + 1)}>{count}</button>
}

function App() {
  const [count, setCount] = useState(0)
  const [text, setText] = useState('hello')
  const [list, setList] = useState([])
  const [y] = useWindowScroll()
  function test() {
    setCount(20)
    setText('jone')
    setList([1, 2, 3])
  }
  return (
    <div>
      <p>{count}</p>
      <p>{text}</p>
      <p>
        {list.map(item => (
          <span>{item}</span>
        ))}
      </p>
      <p>{list.join('-')}</p>
      <button onClick={test}>test</button>
      {/* <button onClick={() => setCount(count + 1)}>{count}</button> */}
      {/* <button onClick={() => setText(text + ',tom')}>{text}</button> */}

      <span>{y}</span>
      <Counter count={10} />
      <Counter count={20} />
    </div>
  )
}

export default App
