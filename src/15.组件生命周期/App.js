import React from 'react'

class Test extends React.Component {
  // 如果数据是组件的状态需要去影响视图 定义到state中
  // 而如果我们需要的数据状态 不和视图绑定 定义成一个普通的实例属性就可以了
  // state中尽量保持精简
  timer = null
  componentDidMount() {
    this.timer = setInterval(() => {
      console.log('定时器开启')
    }, 1000)
  }
  componentWillUnmount() {
    console.log('componentWillUnmount')
    // 清除定时器
    clearInterval(this.timer)
  }
  render() {
    return <div>hello</div>
  }
}

class App extends React.Component {
  constructor() {
    super()
    console.log('constructor')
  }
  state = {
    count: 1,
    flag: true
  }
  componentDidMount() {
    console.log('componentDidMount')
    // 用于发送请求/DOM操作 类似于vue中的mounted
  }
  componentDidUpdate() {
    console.log('componentDidUpdate')
  }
  clickHandler = () => {
    this.setState({
      count: this.state.count + 1,
      flag: !this.state.flag
    })
  }
  render() {
    console.log('render')
    return (
      <>
        <div>this is div</div>
        {this.state.flag ? <Test /> : null}
        <button onClick={this.clickHandler}>{this.state.count}</button>
      </>
    )
  }
}

export default App
