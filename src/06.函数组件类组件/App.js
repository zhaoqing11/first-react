import './app.css'
import React from 'react'

// 创建函数组件
function ComponentTest() {
  return <div>好困</div>
}

// 创建类组件
class HelloComponent extends React.Component {
  render() {
    return <div>想睡觉</div>
  }
}

function App() {
  return (
    <div className="App">
      {/* 渲染函数组件 */}
      <ComponentTest />
      <ComponentTest></ComponentTest>

      {/* 渲染类组件 */}
      <HelloComponent />
    </div>
  )
}

export default App
